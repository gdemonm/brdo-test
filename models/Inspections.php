<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inspections".
 *
 * @property string $id
 * @property int $internal_id
 * @property string $last_modify
 * @property string $regulator
 */
class Inspections extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspections';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           [['id', 'internal_id', 'last_modify', 'regulator'], 'required'],
           [['internal_id'], 'integer'],
           [['last_modify'], 'safe'],
           [['regulator'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'internal_id' => 'Internal ID',
            'last_modify' => 'Last Modify',
            'regulator' => 'Regulator',
        ];
    }
}
