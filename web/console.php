<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require_once __DIR__ . '/../functions.php';
$config = require(__DIR__ . '/../config/console.php');

if( isset($_GET['r']) and !empty($_GET['r'])){

    $_SERVER['argv'] =  [
        $_SERVER['DOCUMENT_ROOT'].'/yii',
        $_GET['r']
    ];
    $_SERVER['argc'] = 2;
}

$application = new yii\console\Application($config);

$exitCode = $application->run();
exit($exitCode);