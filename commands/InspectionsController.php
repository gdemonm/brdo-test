<?php


namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\httpclient\Client;
use app\models\Inspections;

class InspectionsController extends Controller
{


    public function actionCreate()
    {

        $count_pages = 2;
        $page = 1;

        while ($page <= $count_pages) {

            $url ='&page='.$page.'&pageSize=50&orderBy=desc';
            $page++;

            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('get')
                ->setUrl('http://api.ias.brdo.com.ua/v1_1/inspections?apiKey=360d858edaac8313a73d237f340138c097ab6304'.$url)
                ->send();

            if ($response->isOk) {

                if(!isset($response->data['status'])&&$response->data['status']!='success'){
                    echo "Ошибка получения данных\n";
                } elseif($response->data['items']) {

                    foreach ($response->data['items'] as $item){

                        $item['last_modify'] = Yii::$app->formatter->asDateTime(\DateTime::createFromFormat('d-m-Y H:i', $item['last_modify']), 'php:Y-m-d H:i:s');

                        $inspection_item = Inspections::findOne($item['id']);

                        if(!$inspection_item){
                            $model = new Inspections();
                            $model->id = $item['id'];
                            $model->internal_id = $item['internal_id'];
                            $model->last_modify = $item['last_modify'];
                            $model->regulator = $item['regulator'];

                            $model->save();

                        }

                    }

                }

            } else {
                echo "error API\n";
            }
        }

    }

}