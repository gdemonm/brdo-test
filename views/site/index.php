<?php

use yii\grid\GridView;

/* @var $this yii\web\View */

$this->title = 'My Inspections';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h2>Inspections</h2>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        'internal_id',
                        ['attribute' => 'last_modify', 'format' => ['date', 'php:d-m-Y H:i']],
                        'regulator',
                    ],
                ]); ?>

            </div>

        </div>

    </div>
</div>
